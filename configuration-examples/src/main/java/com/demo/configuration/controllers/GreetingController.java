package com.demo.configuration.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RefreshScope
public class GreetingController {
    @Value("${props.greeting.message}") // reading values from application.yml file
    private String greetingMessage;

    @Value("value pass directly ") // same as = "some String"
    private String staticGreetingMessage;

    @Value("${props.greeting.message.not.exists: default value }") // right side of the : will work as a fall back value
    private String defaultGreetingMessage;

    @Value("${props.list.values}") // spring will create a list from the comma separated values
    private List<String> listGreetingMessage;

    @Value("#{${props.db.connection}}") // mapping key value pairs into a map object
    private Map<String, String> dbProps;

    @GetMapping("/greeting")
    public String getGreetingMessage() {
        return String.format("%s | %s | %s | %s | %s", greetingMessage, staticGreetingMessage, defaultGreetingMessage, listGreetingMessage, dbProps);
    }
}
