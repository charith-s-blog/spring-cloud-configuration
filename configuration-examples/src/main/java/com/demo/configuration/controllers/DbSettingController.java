package com.demo.configuration.controllers;

import com.demo.configuration.dto.DbSettingDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/db")
public class DbSettingController {

    @Autowired
    private DbSettingDto dbSettingDto;

    @GetMapping("/setting")
    public String getSeString() {
        return String.format("%s | %s | %s ", dbSettingDto.getConnection(), dbSettingDto.getHost(), dbSettingDto.getPort());
    }
}
